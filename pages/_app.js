import React from "react";
import Navigation from "../components/Navigation";
import "../styles/App.css";
import "../styles/global.css";
import "../styles/Home.css";
import "../styles/Navigation.css"
import "../styles/GirlWithPhone.css";
import "../styles/Form.css";
import "../styles/Register.css";
import "../styles/Login.css"
import "../styles/Input.css"
import "../styles/Soluciones.css"
import "../styles/CrearSoluciones.css"
import {Cookies } from 'react-cookie'
const cookie = new Cookies()
import App from 'next/app'

class OurApp extends App {

  static async getInitialProps({ Component, router, ctx }) {
    let pageProps = {}

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }

    return { pageProps }
  }

  

  render() {


    const { Component, pageProps, router } = this.props

    return <div><Navigation token={cookie.get('token')}/><Component {...pageProps} /></div>
  }
}

export default OurApp



