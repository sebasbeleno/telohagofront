import React from 'react';
import {Jumbotron, Container, Button, Row, Col} from 'react-bootstrap';
import { ImLocation, ImCoinDollar } from 'react-icons/im';
import { FaRegCalendarAlt } from 'react-icons/fa';



function CrearSolucion() {
  return (
    <Container>
      <Jumbotron>
        <h1 className="h1">Crear Solución</h1>
        <h2 className="h2">Trabajo</h2>
        <p>Descripción corta</p>
        <hr className= "line"></hr>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris volutpat enim ipsum, non mattis sapien rhoncus in. Curabitur eleifend diam eget diam hendrerit, quis congue felis eleifend. Vestibulum dui sapien, aliquet nec vehicula at, fermentum ut mi. Mauris feugiat efficitur urna, eget accumsan augue commodo quis. Suspendisse potenti. Sed vitae lorem lobortis, egestas eros vel, maximus arcu. Nullam vel pharetra odio, in viverra lorem. In nunc augue, molestie porta rhoncus a, accumsan non magna. Ut erat est, iaculis ut urna in, lobortis pellentesque nulla. Suspendisse fermentum mauris sagittis ipsum pulvinar, eget porttitor risus bibendum. 
        </p>
        <form>
        <Row>
          <Col>
            <label className="l"><ImCoinDollar /></label>
            <input className="bottomInput" type="number" ></input>
          </Col>
          <Col>
            <label className="l"><ImLocation /> </label>
            <label className="l">Laureles</label>
          </Col>
          <Col>
            <label className="l"><FaRegCalendarAlt /></label>
            <input className="bottomInput" type="date"></input>
          </Col>
          <Col>
            <Button variant="primary" type="submit" className="buttonSquare">Crear</Button>
          </Col>
        </Row>
        </form>
      </Jumbotron>
    </Container>
  );
}

export default CrearSolucion;
