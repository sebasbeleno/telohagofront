import React from 'react';
import { Jumbotron, Container, Button } from 'react-bootstrap';
import { BiFilter } from 'react-icons/bi';
import { ImCoinDollar } from 'react-icons/im';
import { FaRegCalendarAlt, FaCheck, FaTimes } from 'react-icons/fa';
import { CgProfile } from 'react-icons/cg';
import api from '../../lib/api'
import { handleAuthSSR } from '../../lib/handleAuthSSR'
import SolucionesList from '../../components/SolucionesList'

class Solucion extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      data: null,
      error: null
    }

    this.fetchData()
  }

  fetchData = async () => {
    try {
      const response = await api.soluciones.getSoluciones()
      this.setState({ data: response, loading: false })
    } catch (error) {
      this.setState({ loading: false, error: error })
    }
  }

  render() {
    if (this.state.loading == true) {
      return (
        <h1>loading</h1>
      )
    } else {
      return (
        <Container>
          

          <Jumbotron bsPrefix="jumbo">
            <Button variant="light" className="buscar">Buscar</Button> {' '}
            <Button variant="dark" className="filter"><BiFilter /></Button> {' '}
          </Jumbotron>

          <label className="h1">Titulo</label>
          <hr></hr>
          <label>Decripsion corta</label>


         <SolucionesList soluciones={this.state.data}/>
        </Container>
      );
    }
  }
}

Solucion.getInitialProps = async (ctx) => {
  await handleAuthSSR(ctx)

  return {}
}

export default Solucion;
