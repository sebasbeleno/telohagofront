import React from "react";
import Cartera from "../components/Cartera";
import ilus1 from '../public/images/ilustracion1.png'
import ilus2 from '../public/images/ilustracion2.png'

export default function Home() {
  return (
    <div className="general">

      {
        //Home
      }
      <div id="home">
        <div className="block-one">
          <h1>
            Trabajos y <br />
          soluciones,
          <br /> rápidas y seguras.
        </h1>
          <div className="flex lg:w-2/3 w-full sm:flex-row flex-col mx-auto px-8 sm:px-0 items-end">
            <div className="relative sm:mr-4  sm:mb-0 flex-grow w-ful">

              <input type="text" placeholder="Carpintería..." className="w-full bg-white rounded border border-gray-300 focus:border-indigo-500 h-32 text-base outline-none text-gray-700 py-1 px-3 resize-none leading-6 transition-colors duration-200 ease-in-out" />
            </div>
            <button className="text-wgite bg-indigo-500 border-0 py-2 px-8 rounded text-lg text-white">
              Buscar
          </button>
          </div>
        </div>
        <Cartera />
      </div>

      {
        //About us
      }
      <div className="about-us container mx-auto">
        <section class="text-gray-700 body-font">
          <div class="container mx-auto flex px-5 py-24 md:flex-row flex-col items-center">
            <div class="lg:max-w-lg lg:w-full md:w-1/2 w-5/6 mb-10 md:mb-0 rounded-md">
              <img className="rounded-md shadow-lg" src={ilus1}></img>
            </div>
            <div class="lg:flex-grow md:w-1/2 lg:pl-24 md:pl-16 flex flex-col md:items-start md:text-left items-center text-center">
              <h1 class="title-font sm:text-4xl text-3xl mb-4 font-medium text-gray-900">Quienes somos</h1>
              <p class="mb-8 leading-relaxed">TeLoHago fue fundado por un grupo de estudiantes de Ingenieria de Sistemas de la universidad EAFIT.</p>
            </div>
          </div>
        </section>
        <section class="text-gray-700 body-font">
          <div class="container mx-auto flex px-5 py-24 md:flex-row flex-col items-center">
            <div class="lg:flex-grow md:w-1/2 lg:pl-24 md:pl-16 flex flex-col md:items-start md:text-left items-center text-center">
              <h1 class="title-font sm:text-4xl text-3xl mb-4 font-medium text-gray-900">Que hacemos</h1>
              <p class="mb-8 leading-relaxed">TeLoHago es una plataforma para facilitar y centralizar la busqueda de trabajos y soluciones para los problemas del dia a dia.</p>
            </div>
            <div class="lg:max-w-lg lg:w-full md:w-1/2 w-5/6 mb-10 md:mb-0 rounded-md">
              <img src={ilus2} className="rounded-md shadow-lg"></img>
            </div>
          </div>
        </section>

       
      </div>

      {
        //End
      }
    </div>
  );
}
