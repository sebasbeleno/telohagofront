import React from "react";
import Form from "../../components/LoginForm"
import GirlWithPhone from "../../components/GirlWithPhone";
import { handleAuthSSR } from '../../lib/handleAuthSSR'

function Login() {
    const url = ""
    const title = "Es un gusto verte de nuevo"
    const fields = ["Usuario", "Contraseña"]
    const buttonName = "Login"
    return <div id="login">
        <Form title={title} fields={fields} buttonName={buttonName} url={url} />
        <GirlWithPhone />
    </div>;
}

Login.getInitialProps = async (ctx) => {
    await handleAuthSSR(ctx)

    return {}
}

export default Login
