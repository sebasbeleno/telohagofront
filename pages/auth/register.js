import React from "react";
import GirlWithPhone from "../../components/GirlWithPhone";
import Form from "../../components/RegisterForm"


export default function Login() {
    const url = "";
    const title = "Es un gusto tenerte con nosotros"
    const fields = ["Nombres", "Correo electronico", "Usuario", "Contraseña", "Confirma la contraseña"]
    const buttonName = "Registrarse"
    return <div id="register">
        <Form title={title} fields={fields} buttonName={buttonName} url={url} />
        <GirlWithPhone />
    </div>;
}