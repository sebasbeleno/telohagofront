import React from "react";
import Router from 'next/router'
import { handleAuthSSR } from '../../lib/handleAuthSSR'
import { Cookies } from 'react-cookie'

const cookies = new Cookies()

class Login extends React.Component {

    constructor(props) {
        super(props);
        
     
    }
    componentDidMount(){
        cookies.remove('token')
        Router.replace('/')
	}

    

    render() {
        return <h1>Saliendo...</h1>
    }
}

Login.getInitialProps = async (ctx) => {
    await handleAuthSSR(ctx)

    return {}
}

export default Login
