import React from 'react';
import { handleAuthSSR } from '../../lib/handleAuthSSR'
import api from '../../lib/api'
import ProblemasList from '../../components/ProblemasList'
import Link from 'next/link';




class Problemas extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      data: null,
      error: null
    }
    this.fetchData()
  }

  fetchData = async () => {
    try {
      const response = await api.problemas.getProblemas()
      this.setState({ data: response, loading: false })
    } catch (error) {
      this.setState({ loading: false, error: error })
    }
  }



  render() {
    if (this.state.loading == true) {
      return (
        <h1>loading</h1>
      )
    } else {
      return (
        <div className="container mx-auto">
          <div className="jumbo flex justify-between">
            <Link href="/problema/crearProblema">
              <button className="text-indigo-700 bg-white border-0 py-2 px-12  focus:outline-none hover:bg-indigo-600 rounded text-lg">Crear</button>
            </Link>
            <div className="flex md:flex-no-wrap flex-wrap justify-center items-end md:justify-start">
              <div class="relative sm:w-64 w-40 sm:mr-4 mr-2">
                <input type="text" id="footer-field" name="footer-field" class="w-full bg-gray-100 rounded border border-gray-300 focus:border-indigo-500 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out" />
              </div>
              <button class="inline-flex text-white bg-indigo-500 border-0 py-2 px-6 focus:outline-none hover:bg-indigo-600 rounded">Button</button>
            </div>
          </div>


          <ProblemasList problemas={this.state.data} />
        </div>
      );
    }
  }
}

Problemas.getInitialProps = async (ctx) => {
  await handleAuthSSR(ctx)

  return {}
}


export default Problemas;
