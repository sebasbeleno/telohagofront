import React from 'react';
import { handleAuthSSR } from '../../lib/handleAuthSSR'
import api from '../../lib/api'
import CrearProblemaCard from '../../components/CrearProblemaCard'



class CrearProblema extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      data: null,
      error: null
    }
  }

 

  render(){
    return(
      <CrearProblemaCard/>
    )
  }
}

CrearProblema.getInitialProps = async (ctx) => {
  await handleAuthSSR(ctx)

  return {}
}

export default CrearProblema;