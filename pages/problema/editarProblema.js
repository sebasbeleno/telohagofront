import React from 'react';
import {Jumbotron, Container, Button, Row, Col} from 'react-bootstrap';
import { ImLocation, ImCoinDollar } from 'react-icons/im';
import { FaRegCalendarAlt } from 'react-icons/fa';
import { handleAuthSSR } from '../../lib/handleAuthSSR'
import api from '../../lib/api'
import CrearProblemaCard from '../../components/CrearProblemaCard'



class CrearProblema extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      data: null,
      error: null
    }
    this.fetchData()
  }

  fetchData = async () => {
    try {
      const response = await api.problemas.getEditarProblema()
      this.setState({ data: response, loading: false })
    } catch (error) {
      this.setState({ loading: false, error: error })
    }
  }

  render(){
      if (this.state.loading == true) {
        return (
          <h1>loading</h1>
        )
      }else{
        return(
          <CrearProblemaCard problema={this.state.data}/>
        )
      }
  }
}

CrearProblema.getInitialProps = async (ctx) => {
  await handleAuthSSR(ctx)

  return {}
}

export default CrearProblema;