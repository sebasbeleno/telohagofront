import React from 'react';
import { handleAuthSSR } from '../../lib/handleAuthSSR'
import api from '../../lib/api'
import ProblemaCard from '../../components/ProblemaCard'


class Problema extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      data: null,
      error: null
    }
    this.fetchData()
  }

  fetchData = async () => {
    try {
      const response = await api.problemas.getProblema()
      this.setState({ data: response, loading: false })
    } catch (error) {
      this.setState({ loading: false, error: error })
    }
  }

  render(){
    if (this.state.loading == true) {
      return (
        <h1>loading</h1>
      )
    } else {
      return (
        <ProblemaCard problema={this.state.data}/>
      )
    }
  }
}

Problema.getInitialProps = async (ctx) => {
  await handleAuthSSR(ctx)

  return {}
}

export default Problema;
