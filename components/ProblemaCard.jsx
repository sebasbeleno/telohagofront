import { ImLocation, ImCoinDollar } from "react-icons/im";
import { FaRegCalendarAlt } from "react-icons/fa";
import { CgProfile } from "react-icons/cg";

class ProblemaCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      problema: this.props.problema[0],
      show: false
    };

    console.log(this.state.problema);
  }

  render() {
    return (
      <div className="container mx-auto">
        <div className="solucion shadow-2xl">
          <div className="pull-right">
            <label>
              <CgProfile />
            </label>
            <label>Autor</label>
          </div>
          <h2 className="text-5xl font-bold text-gray-800 ">
            {this.state.problema.titulo}
          </h2>
          <hr className="line"></hr>
          <div className="w-3/6  my-5">
            <p>{this.state.problema.descripcion}</p>
          </div>
          <form>
            <div className="grid grid-cols-4 auto-rows-max">
              <div className="flex">
                <label className="l mr-5">
                  <ImCoinDollar
                    value={{ style: { verticalAlign: "middle" }, size: "3em" }}
                  />
                </label>
                <label className="">{this.state.problema.pago}</label>
              </div>
              <div className="flex">
                <label className="l mr-5">
                  <ImLocation
                    value={{ style: { verticalAlign: "middle" }, size: "3em" }}
                  />{" "}
                </label>
                <label className="">{this.state.problema.cuidad}</label>
              </div>
              <div className="flex" mr-5>
                <label className="l">
                  <FaRegCalendarAlt
                    value={{ style: { verticalAlign: "middle" }, size: "3em" }}
                  />
                </label>
                <label className="">{this.state.problema.fecha}</label>
              </div>
              <div></div>
              <button
                onClick={() => this.setState({ show: true })}
                className="buttonSquare"
              >
                Contactar
              </button>

              
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default ProblemaCard;
