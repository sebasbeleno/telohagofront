import React, { useState } from "react";
import { Cookies } from "react-cookie";
import Router from "next/router";
import api from "../lib/api";

const cookies = new Cookies();

class LoginForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      error: null,
      usuario: "",
      password: "",
      token: cookies.get("token") || null,
    };

    this.handleChangeUsername = this.handleChangeUsername.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.handleSummit = this.handleSummit.bind(this);
  }

  handleChangeUsername() {
    this.setState({ usuario: event.target.value });
  }

  handleChangePassword() {
    this.setState({ password: event.target.value });
  }

  handleSummit = async (e) => {
    e.preventDefault();
    this.setState({ loading: true });

    try {
      console.log("a...");
      const response = await api.auth.login(
        this.state.usuario,
        this.state.password
      );

      cookies.set("token", response);

      Router.push({ pathname: `/problema` });
    } catch (r) {
        this.setState({ loading: false, error: r });
    }
  };

  render() {
    return (
      <div id="form-outer">
        <form className="form">
          <h1 className="font-bold text-5xl text-gray-800 mb-10">Login</h1>

          <input
            name="usuario"
            id="usuario"
            type="text"
            placeholder="Usuario..."
            onChange={this.handleChangeUsername}
            className="w-full bg-white rounded border border-gray-300 focus:border-indigo-500 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
          ></input>
          <input
            name="password"
            id="password"
            type="password"
            placeholder="Password..."
            onChange={this.handleChangePassword}
            className="w-full bg-white rounded border border-gray-300 mt-5 focus:border-indigo-500 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
          ></input>

          <input
            type="button"
            onClick={this.handleSummit}
            value="Enviar..."
            className="text-white bg-indigo-500 border-0 py-2 px-12 w-full mt-5 focus:outline-none hover:bg-indigo-600 rounded text-lg"
          />
          
          {this.state.error &&
            <p className="mt-5 text-red-400	">Datos incorrrectos :(</p>
          }
        </form>
      </div>
    );
  }
}

export default LoginForm;
