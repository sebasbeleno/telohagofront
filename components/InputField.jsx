import React, { useEffect, useRef } from 'react'


export default function InputField({ wasSubmitted, field, fieldState, setFieldsState }) {
    const inputRef = useRef()
    
    useEffect(() => {
        if (wasSubmitted) {
            const changeText = () => {
                setFieldsState(() => ({ ...fieldState, field: inputRef.value }));
            }
            changeText()
        }
    }, [wasSubmitted]);

    
    return (
        <input
            ref={inputRef}
            className="input-form"
            type="text"
            placeholder={`     ${field}`}
        />
    )
}
