import { Jumbotron, Button } from 'react-bootstrap';
import { ImCoinDollar } from 'react-icons/im';
import { FaRegCalendarAlt, FaCheck, FaTimes } from 'react-icons/fa';
import { CgProfile } from 'react-icons/cg';

class SolucionCard extends React.Component {
  constructor(props) {
      super(props)
  }

  render() {
    return (
      <Jumbotron bsPrefix="solucion">
        <label>
          <CgProfile />
        </label>
        <label>  {this.props.valor.autor} </label>
        <Button className="pull-right" variant="success">
          {" "}
          <FaCheck />{" "}
        </Button>{" "}
        <br></br>
       
        <label className="">
          <ImCoinDollar />
        </label>
        <input className="input" type="number"></input>
        <label className="">
          <FaRegCalendarAlt />
        </label>
        <input className="input" type="date"></input>
        <Button className="pull-right" variant="danger">
          <FaTimes />
        </Button>{" "}
      </Jumbotron>
    );
  }
}

class SolucionesList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      soluciones: this.props.soluciones,
    };
  }

  render() {
    return (
      <>
        {this.state.soluciones.map((solucion) => {
          return <SolucionCard valor={solucion} />;
        })}
      </>
    );
  }
}

export default SolucionesList;
