import { ImLocation, ImCoinDollar } from "react-icons/im";
import { FaRegCalendarAlt } from "react-icons/fa";
import api from "../lib/api";
import Router from "next/router";

class CrearProblemaCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      titulo: "",
      desct: "",
      des: "",
      pago: "",
      lugar: "",
      fecha: "",
    };
    this.handleChangeTitulo = this.handleChangeTitulo.bind(this);
    this.handleChangeDesct = this.handleChangeDesct.bind(this);
    this.handleChangeDes = this.handleChangeDes.bind(this);
    this.handleChangePago = this.handleChangePago.bind(this);
    this.handleChangeLugar = this.handleChangeLugar.bind(this);
    this.handleChangeFecha = this.handleChangeFecha.bind(this);
    this.handleSummit = this.handleSummit.bind(this);
  }

  handleChangeTitulo() {
    this.setState({ titulo: event.target.value });
  }

  handleChangeDesct() {
    this.setState({ desct: event.target.value });
  }

  handleChangeDes() {
    this.setState({ des: event.target.value });
  }

  handleChangePago() {
    this.setState({ pago: event.target.value });
  }

  handleChangeLugar() {
    this.setState({ lugar: event.target.value });
  }

  handleChangeFecha() {
    this.setState({ fecha: event.target.value });
  }

  handleSummit = async (e) => {
    e.preventDefault();
    this.setState({ loading: true });

    try {
      const response = await api.problemas.getCrearProblema(
        7,
        this.state.titulo,
        this.state.des,
        this.state.fecha,
        this.state.pago,
        this.state.lugar
      );

      Router.push({ pathname: `/problema` });
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    return (
      <div className="container mx-auto">
        <div className="solucion shadow-2xl">
          <h1 className="text-5xl font-bold text-gray-700 mb-5">
            Crear Trabajo
          </h1>

          <input
            type="text"
            placeholder="Titulo"
            className="tituloInput"
            value={this.state.titulo}
            onChange={this.handleChangeTitulo}
          ></input>
          <br></br>
          <input
            type="text"
            placeholder="Descripción Corta"
            className="descripciionCortaInput"
            value={this.state.desct}
            onChange={this.handleChangeDesct}
          ></input>
          <hr className="line"></hr>
          <textarea
            type="text"
            placeholder="Descripción del Trabajo"
            className="descripciionInput"
            value={this.state.des}
            onChange={this.handleChangeDes}
          ></textarea>
          <form>
            <div className="grid grid-cols-4 auto-rows-max">
              <div className="flex">
                <label className="l mr-5">
                  <ImCoinDollar
                    value={{ style: { verticalAlign: "middle" }, size: "3em" }}
                  />
                </label>
                <input
                  className="bottomInput"
                  type="number"
                  placeholder="Pago"
                  value={this.state.pago}
                  onChange={this.handleChangePago}
                ></input>
              </div>
              <div className="flex">
                <label className="l mr-5">
                  <ImLocation />{" "}
                </label>
                <input
                  className="bottomInput"
                  type="text"
                  placeholder="Lugar"
                  value={this.state.lugar}
                  onChange={this.handleChangeLugar}
                ></input>
              </div>
              <div className="flex ml-10 ">
                <label className="l mr-5">
                  <FaRegCalendarAlt />
                </label>
                <input
                  className="bottomInput"
                  type="date"
                  value={this.state.fecha}
                  onChange={this.handleChangeFecha}
                ></input>
              </div>
              <div>
                <label className="l text-transparent ">
                  <FaRegCalendarAlt />
                </label>
              </div>
                <input
                  type="button"
                  onClick={this.handleSummit}
                  className="buttonSquare mt-5"
                  value="Enviar"
                ></input>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default CrearProblemaCard;
