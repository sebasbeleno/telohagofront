import React, { useState } from "react";
import { Cookies } from "react-cookie";
import Router from "next/router";
import api from "../lib/api";

const cookies = new Cookies();

class RegisterForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      error: null,
      name: "",
      email: "",
      usuario: "",
      password: "",
      cuidad: "",
      token: cookies.get("token") || null,
    };

    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangeUsername = this.handleChangeUsername.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.handleChangeConfirmPassword = this.handleChangeConfirmPassword.bind(
      this
    );
    this.handleSummit = this.handleSummit.bind(this);
  }

  handleChangeName() {
    this.setState({ name: event.target.value });
  }

  handleChangeEmail() {
    this.setState({ email: event.target.value });
  }

  handleChangeUsername() {
    this.setState({ usuario: event.target.value });
  }

  handleChangePassword() {
    this.setState({ password: event.target.value });
  }

  handleChangeConfirmPassword() {
    this.setState({ cuidad: event.target.value });
  }

  handleSummit = async (e) => {
    e.preventDefault();
    this.setState({ loading: true });

    console.log(this.state)

    try {
      const response = await api.auth.register(
        this.state.name,
        this.state.email,
        this.state.usuario,
        this.state.password,
        this.state.usuario
      );

      cookies.set("token", response);

      Router.push({ pathname: `/problema` });
    } catch (error) {
      this.setState({ loading: false, error: error });
    }
  };

  render() {
    return (
      <div id="form-outer">
        <form className="form">
          <h1 className="font-bold text-5xl text-gray-800 mb-10">Register</h1>

          <input
            name="Nombre"
            id="nombre"
            type="text"
            placeholder="Usuario"
            onChange={this.handleChangeName}
            className="w-full bg-white rounded border border-gray-300 focus:border-indigo-500 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
          ></input>
          <input
            name="Email"
            id="Email"
            type="text"
            placeholder="Correo"
            onChange={this.handleChangeEmail}
            className="w-full bg-white rounded border border-gray-300 mt-5 focus:border-indigo-500 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
          ></input>
          <input
            name="usuario"
            id="usuario"
            type="text"
            placeholder="Usuario"
            onChange={this.handleChangeUsername}
            className="w-full bg-white rounded border border-gray-300 mt-5 focus:border-indigo-500 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
          ></input>
          <input
            name="password"
            id="password"
            type="password"
            placeholder="Contraseña"
            onChange={this.handleChangePassword}
            className="w-full bg-white rounded border border-gray-300 mt-5 focus:border-indigo-500 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
          ></input>
          <input
            name="Cuidad"
            id="Cuidad"
            type="text"
            placeholder="Cuidad"
            onChange={this.handleChangeConfirmPassword}
            className="w-full bg-white rounded border border-gray-300 mt-5 focus:border-indigo-500 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
          ></input>

          <input
            type="button"
            onClick={this.handleSummit}
            value="Registrarse..."
            className="text-white bg-indigo-500 border-0 py-2 px-12 w-full mt-5 focus:outline-none hover:bg-indigo-600 rounded text-lg"
          />
        </form>
      </div>
    );
  }
}

export default RegisterForm;
