import { CgProfile } from "react-icons/cg";
import Link from 'next/link'

class ProblemaCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      problema: this.props.problema,
    };
  }

  render() {
    return (
      <div className="solucion shadow-2xl">
        <label className="text-5xl font-bold">{this.state.problema.titulo}</label>
        <div className="pull-right">
          <label>
            <CgProfile />
          </label>
          <label>{this.state.problema.autor}</label>
        </div>
        <br></br>
        <label className="">{this.state.problema.Desct}</label>
        <br></br>
        <Link
          href="problema/p/[id_problema]"
          as={`/problema/p/${this.state.problema.id_problema}`}
        >
          <a variant="primary" className="text-white bg-indigo-500 border-0 py-2 px-12 w-full mt-5 focus:outline-none hover:bg-indigo-600 rounded text-lg">
            Ver Trabajo
          </a>
        </Link>{" "}
      </div>
    );
  }
}

class ProblemasList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      problemas: this.props.problemas,
    };
  }

  render() {
    return (
      <>
        {this.state.problemas.map((problema) => {
          return <ProblemaCard problema={problema} />;
        })}
        {this.state.problemas.length <= 0 &&
          <div>
            <h1 className="text-5xl mt-10 font-bold text-indigo-700	">No hay nada que mostrar :c</h1>
          </div>
        }
      </>
    );
  }
}

export default ProblemasList;
