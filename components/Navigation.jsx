import React from "react";
import Link from "next/link";
import ThumbsUp from "./ThumbsUp";
import { Cookies } from "react-cookie";

const cookie = new Cookies();

export default class Navigation extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      token: cookie.get("token"),
    };

    console.log(this.props.token);
  }

  render() {
    if (this.state.token == undefined || this.state.token == '') {
      return (
        <header class="text-blue-600	 body-font">
          <div class="container mx-auto flex flex-wrap p-5 flex-col md:flex-row items-center">
            <a class="flex title-font font-medium items-center text-gray-900 mb-4 md:mb-0">
              <ThumbsUp />
              <Link href="/">
                <span class="ml-3 text-xl">TeLoHago</span>
              </Link>
            </a>
            <nav class="md:ml-auto flex flex-wrap items-center text-base justify-center">
              <Link href="/auth/login">
                <a class="cursor-pointer mr-5 hover:text-blue-500	">Ingresar</a>
              </Link>
              <Link href="/auth/register">
                <a class="cursor-pointer mr-5 hover:text-blue-500	">
                  Registrarse
                </a>
              </Link>
            </nav>
          </div>
        </header>
      );
    }

    return (
      <header class="text-blue-600	 body-font">
        <div class="container mx-auto flex flex-wrap p-5 flex-col md:flex-row items-center">
          <a class="flex title-font font-medium items-center text-gray-900 mb-4 md:mb-0">
            <ThumbsUp />
            <Link href="/problema">
              <span class="ml-3 text-xl">TeLoHago</span>
            </Link>
          </a>
          <nav class="md:ml-auto flex flex-wrap items-center text-base justify-center">
            <Link href="/auth/logout">
              <a class="cursor-pointer mr-5 hover:text-blue-500	">Salir</a>
            </Link>
            <Link href="/problema">
              <a class="cursor-pointer mr-5 hover:text-blue-500	">Problemas</a>
            </Link>
          </nav>
        </div>
      </header>
    );
  }
}

