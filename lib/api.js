import axios from 'axios';

let BASE_URL = ""

BASE_URL = "http://localhost:3030"

async function postCallAuth(endpoint, usuario, password) {


    let url = BASE_URL + endpoint

    url = encodeURI(url)

    const response = await axios({
        method: 'post',
        url: url,
        params: {
            usuario: usuario,
            contraseña: password
        }
    })

    console.log(response)

    return response.data
}

async function postCallAuthSingUp(endpoint, nombre, usuario, correo, password, cuidad) {



    let url = BASE_URL + endpoint

    url = encodeURI(url)
    console.log(url)

    const response = await axios({
        method: 'post',
        url: url,
        params: {
            usuario: usuario,
            nombre: nombre,
            correo: correo,
            password: password,
            cuidad: cuidad
        }
    })

    console.log(response)

    return response.data
}

async function getCall(endpoint, usuario, password) {


    let url = BASE_URL + endpoint

    url = encodeURI(url)

    const response = await axios({
        method: 'get',
        url: url,
    })


    return response.data
}

async function getCallProblema(endpoint, id) {


    let url = BASE_URL + endpoint

    url = encodeURI(url)

    const response = await axios({
        method: 'get',
        url: url,
        params: {
            id_problema: id
        }
    })


    return response.data
}
async function postCallProblema(endpoint, id_user, titulo, desc, fecha, pago, cuidad) {

    
    let url = BASE_URL + endpoint

    url = encodeURI(url)


    const response = await axios({
        method: 'post',
        url: url,
        params: {
            id_user: id_user,
            titulo: titulo,
            desc: desc,
            fecha: fecha,
            pago: pago,
            cuidad: cuidad
        }
    })


    return response.data
}

const api = {
    auth: {
        login(usuario, password) {
            console.log("a")
            return postCallAuth('/auth/login', usuario, password)
        },
        register(nombre, correo, usuario, password, cuidad) {
            return postCallAuthSingUp('/auth/singup', nombre, usuario, correo, password, cuidad)
        }
    },

    problemas: {
        getProblemas() {
            return getCall('/problemas')
        },
        getProblema(id) {
            return getCallProblema('/problemas/get', id)
        },
        getCrearProblema(id_user, titulo, desc, fecha, pago, cuidad) {
            return postCallProblema('/problemas/crear', id_user, titulo, desc, fecha, pago, cuidad)
        },
        getEditarProblema() {
            return {
                "titulo": "Necesito albañol",
                "Desct": "hgola",
                "Des": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris volutpat enim ipsum, non mattis sapien rhoncus in. Curabitur eleifend diam eget diam hendrerit, quis congue felis eleifend. Vestibulum dui sapien, aliquet nec vehicula at, fermentum ut mi. Mauris feugiat efficitur urna, eget accumsan augue commodo quis. Suspendisse potenti. Sed vitae lorem lobortis, egestas eros vel, maximus arcu. Nullam vel pharetra odio, in viverra lorem. In nunc augue, molestie porta rhoncus a, accumsan non magna. Ut erat est, iaculis ut urna in, lobortis pellentesque nulla. Suspendisse fermentum mauris sagittis ipsum pulvinar, eget porttitor risus bibendum.",
                "autor": "Sebas",
                "pago": "1",
                "lugar": "Laureles",
                "fecha": "12-12-12"
            }
        }
    },

    soluciones: {
        getSoluciones() {
            return [
                {
                    "titulo": "Necesito albañol",
                    "Desct": "hgola",
                    "autor": "Sebas",
                    "fecha": "12-12-12",
                    "pago": "1USD"
                },
                {
                    "titulo": "sdfsdffsdfsd",
                    "Desct": "hgola",
                    "autor": "Alejo",
                    "fecha": "12-12-12",
                    "pago": "1USD"
                },
                {
                    "titulo": "Holfsdfdsfsdfsdfa",
                    "Desct": "hgola",
                    "autor": "Julián",
                    "fecha": "12-12-12",
                    "pago": "1USD"
                },
                {
                    "titulo": "dsfsdfsffsdf",
                    "Desct": "hgola",
                    "autor": "Mario",
                    "fecha": "12-12-12",
                    "pago": "1USD"
                }
            ]

        }
    }
}

export default api;