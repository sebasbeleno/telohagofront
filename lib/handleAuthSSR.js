import { createContext } from 'react';
import { Cookies } from 'react-cookie'
const cookies = new Cookies();
import Router from 'next/router'

export async function handleAuthSSR(ctx) {

    let token = null;

    if (ctx.req) {
        console.log(":(")
        if (ctx.req.headers.cookie == undefined || ctx.req.headers.cookie == null) {
            token = "TokenNoProvided"
        } else {
            token = ctx.req.headers.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1")
        }
    } else {
        console.log(":D")
        token = cookies.get('token')
    }


    if (ctx.pathname === '/auth/login') {
        /**
         * No auth
         */
        if (token == "TokenNoProvided" || token == undefined) {
            return
        } else {
            /**
         * Sí auth :D
         */
            if (ctx.res) {
                ctx.res.writeHead(302, {
                    Location: '/problema'
                })
                ctx.res.end()
            } else {
                Router.push({pathname: `/problema`})
            }
        }


    }



}